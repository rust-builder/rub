//! RUst Builder
#![allow(unstable)]
#![feature(plugin)]
extern crate buildable;
extern crate "rl-sys" as readline;
extern crate repl;
extern crate term;
#[plugin] #[no_link] extern crate peg_syntax_ext;

#[cfg(feature = "rust_rub")] extern crate rust_rub;
#[cfg(feature = "cargo_rub")] extern crate cargo_rub;
#[cfg(feature = "clj")] extern crate clj_rub;
#[cfg(feature = "mongo")] extern crate mongo_rub;
#[cfg(feature = "node")] extern crate node_rub;
#[cfg(feature = "python")] extern crate python_rub;
#[cfg(feature = "rub_rub")] extern crate rub_rub;
#[cfg(feature = "v8")] extern crate v8_rub;

use buildable::LifeCycle;
use repl::Repl;
use reader::expression;
use std::fmt;
use std::os;

use self::ReplErr::{ReadErr,EvalErr,PrintErr};
use self::RubType::*;

#[cfg(feature = "rust_rub")] use rust_rub::RustRub;
#[cfg(feature = "cargo_rub")] use cargo_rub::CargoRub;
#[cfg(feature = "clj")] use clj_rub::CljRub;
#[cfg(feature = "mongo")] use mongo_rub::MongoRub;
#[cfg(feature = "node")] use node_rub::NodeRub;
#[cfg(feature = "python")] use python_rub::PythonRub;
#[cfg(feature = "rub_rub")] use rub_rub::RubRub;
#[cfg(feature = "v8")] use v8_rub::V8Rub;

static USAGE: &'static str = "Rust Builder

Usage:
    rub <project> help
    rub <project> [<proj_opts>] [<lifecycle>...]
    rub -i | --interactive
    rub -h | --help
    rub --version

Options:
    -i --interactive  Run rub in interactive mode.
    -h --help         Show this screen.
    --version         Show version.";

include!(concat!(env!("OUT_DIR"), "/version.rs"));

struct RubRepl;

pub enum ReplErr {
    ReadErr(String),
    EvalErr(String),
    PrintErr(String),
}

impl fmt::Display for ReplErr {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let x = match *self {
            ReadErr(ref x)  => x,
            EvalErr(ref x)  => x,
            PrintErr(ref x) => x,
        };
        write!(f, "{}", x)
    }
}

struct RubCmd {
    cmd: String,
    args: Vec<String>,
    actions: Vec<String>,
}

impl RubCmd {
    pub fn new(cmd: String, args: Vec<String>, actions: Vec<String>) -> RubCmd {
        RubCmd {
            cmd: cmd,
            args: args,
            actions: actions,
        }
    }
}

enum RubType {
    Cmd(RubCmd),
    Exit,
    Help,
    List,
    Strn(String),
    Version,
}

impl RubType {
    pub fn print(&self) {
        match *self {
            Exit    => { println!("Exiting..."); },
            Help    => { let repl = RubRepl; repl.help(); },
            List    => {
                println!("Use <mod> help for more information");
                for module in list().iter() {
                    println!("{}", module);
                }
            },
            Version => { println!("{}", version()); },
            _       => {},
        }
    }
}

type RubResult = Result<RubType,ReplErr>;

peg! reader(r#"
use super::RubCmd;

#[pub]
expression -> RubCmd
    = c:name a:([ ] args)? t:([ ] actions)? {
        let args = a.unwrap_or(Vec::new());
        let actions = t.unwrap_or(Vec::new());

        RubCmd::new(c, args, actions)
    }

actions -> Vec<String>
    = name ++ " "

args  -> Vec<String>
    = arg ++ " "

arg -> String
    = shortarg / longarg

shortarg -> String
    = [-][a-zA-Z]+ { match_str.to_string() }

longarg -> String
    = [-]{2}[a-z][a-z0-9_\-=,]+ { match_str.to_string() }

name -> String
    = [a-z][a-z0-9_\-\.]* { match_str.to_string() }
    / [0-9\.]+            { match_str.to_string() }
    / [\?]                { match_str.to_string() }

"#);

fn rubcmd_to_vec<'a>(proj: &str,
                     args: Vec<String>,
                     actions: Vec<String>,
                     vec: &'a mut Vec<String>) -> &'a mut Vec<String> {
    vec.push("rub".to_string());
    vec.push(proj.to_string());
    vec.push_all(args.as_slice());
    vec.push_all(actions.as_slice());
    vec
}

impl Repl<RubType,ReplErr> for RubRepl {
    fn preamble(&self, _: bool) -> &RubRepl {
        let mut t = term::stdout().unwrap();
        t.attr(term::attr::Bold).unwrap();
        t.fg(term::color::GREEN).unwrap();
        let head = format!("rub {} - Interactive Mode\n", branch());
        t.write_str(head.as_slice()).unwrap();
        t.reset().unwrap();
        (writeln!(t, "Enter 'help' or '?' to get started.")).unwrap();
        t.flush().unwrap();

        let hist_file = Path::new(env!("HOME")).join(".rub-rl-hist");
        readline::preload_history(&hist_file);
        self
    }

    fn help(&self) {
        println!("Rub REPL");
        println!("");
        println!("help | ?       - Show this message");
        println!("exit | quit    - Exit the REPL");
        println!("list | ls      - List the supported modules");
        println!("version        - Show the rub version information");
        println!("<module> <cmd> - Execute the given command");
        println!("                 for the given module, i.e. cargo help");
    }

    fn read(&self, input: String) -> RubResult {
        match expression(input.as_slice()) {
            Ok(x)  => Ok(Cmd(x)),
            Err(e) => Err(ReadErr(e)),
        }
    }

    fn eval(&self, ast: RubType) -> RubResult {
        let mut v = Vec::new();

        match ast {
            Cmd(RubCmd{cmd, args, actions}) => {
                match cmd.as_slice() {
                    "exit" | "quit" => Ok(Exit),
                    "help" | "?"    => Ok(Help),
                    "list" | "ls"   => Ok(List),
                    "version"       => Ok(Version),
                    #[cfg(feature = "cargo_rub")]
                    "cargo"         => {
                        let run = LifeCycle::run_lifecycle;
                        rubcmd_to_vec(cmd.as_slice(), args, actions, &mut v);
                        match run(&v, &mut CargoRub::new()) {
                            Err(e) => Err(EvalErr(e.to_string())),
                            Ok(_)  => Ok(Strn(cmd)),
                        }
                    },
                    #[cfg(feature = "rust_rub")]
                    "rust"          => {
                        let run = LifeCycle::run_lifecycle;
                        rubcmd_to_vec(cmd.as_slice(), args, actions, &mut v);
                        match run(&v, &mut RustRub::new()) {
                            Err(e) => Err(EvalErr(e.to_string())),
                            Ok(_)  => Ok(Strn(cmd)),
                        }
                    },
                    #[cfg(feature = "clj_rub")]
                    "clj"           => {
                        let run = LifeCycle::run_lifecycle;
                        rubcmd_to_vec(cmd.as_slice(), args, actions, &mut v);
                        match run(&v, &mut CljRub::new()) {
                            Err(e) => Err(EvalErr(e.to_string())),
                            Ok(_)  => Ok(Strn(cmd)),
                        }
                    },
                    #[cfg(feature = "mongo_rub")]
                    "mongo"         => {
                        let run = LifeCycle::run_lifecycle;
                        rubcmd_to_vec(cmd.as_slice(), args, actions, &mut v);
                        match run(&v, &mut MongoRub::new()) {
                            Err(e) => Err(EvalErr(e.to_string())),
                            Ok(_)  => Ok(Strn(cmd)),
                        }
                    },
                    #[cfg(feature = "node_rub")]
                    "node"          => {
                        let run = LifeCycle::run_lifecycle;
                        rubcmd_to_vec(cmd.as_slice(), args, actions, &mut v);
                        match run(&v, &mut NodeRub::new()) {
                            Err(e) => Err(EvalErr(e.to_string())),
                            Ok(_)  => Ok(Strn(cmd)),
                        }
                    },
                    #[cfg(feature = "python_rub")]
                    "python"        => {
                        let run = LifeCycle::run_lifecycle;
                        rubcmd_to_vec(cmd.as_slice(), args, actions, &mut v);
                        match run(&v, &mut PythonRub::new()) {
                            Err(e) => Err(EvalErr(e.to_string())),
                            Ok(_)  => Ok(Strn(cmd)),
                        }
                    },
                    #[cfg(feature = "rub_rub")]
                    "rub"           => {
                        let run = LifeCycle::run_lifecycle;
                        rubcmd_to_vec(cmd.as_slice(), args, actions, &mut v);
                        match run(&v, &mut RubRub::new()) {
                            Err(e) => Err(EvalErr(e.to_string())),
                            Ok(_)  => Ok(Strn(cmd)),
                        }
                    },
                    #[cfg(feature = "v8_rub")]
                    "v8"            => {
                        let run = LifeCycle::run_lifecycle;
                        rubcmd_to_vec(cmd.as_slice(), args, actions, &mut v);
                        match run(&v, &mut V8Rub::new()) {
                            Err(e) => Err(EvalErr(e.to_string())),
                            Ok(_)  => Ok(Strn(cmd)),
                        }
                    },
                    _               => Ok(Strn(cmd)),
                }
            },
            _ => Err(EvalErr("Invalid AST".to_string())),
        }
    }

    fn print(&self, exp: RubType) -> RubResult {
        exp.print();
        Ok(exp)
    }

    fn break_loop(&self, cmd: &RubType) -> bool {
        match *cmd {
            Exit => true,
            _    => false,
        }
    }
}

fn version() -> String {
    format!("{} {} rub {}", now(), sha(), branch())
 }

fn usage () -> String {
    format!("{}", USAGE)
}

fn list() -> Vec<String> {
    let mut mods = Vec::new();

    if cfg!(feature = "cargo_rub") {
        mods.push("cargo");
    }
    if cfg!(feature = "rust_rub") {
        mods.push("rust");
    }
    if cfg!(feature = "clj_rub") {
        mods.push("clj");
    }
    if cfg!(feature = "mongo_rub") {
        mods.push("mongo");
    }
    if cfg!(feature = "node_rub") {
        mods.push("node");
    }
    if cfg!(feature = "python_rub") {
        mods.push("python");
    }
    if cfg!(feature = "rub_rub") {
        mods.push("rub");
    }
    if cfg!(feature = "v8_rub") {
        mods.push("v8");
    }

    let mut b: Vec<String> = mods.iter().map(|s| s.to_string()).collect();
    b.sort();
    b
}

fn run(args: &Vec<String>) -> isize {
    let repl = RubRepl;

    let result: Result<u8,u8> = match args.tail().first() {
        Some(h) => {
            match h.as_slice() {
                "--version"     => { println!("{}", version()); Ok(0) },
                "-h" | "--help" => { println!("{}", usage()); Ok(0) },
                "-i" | "--interactive" => {
                    repl.preamble(false)._loop("rub > ", true);
                    Ok(0)
                },
                _               => {
                    let mut argstr = String::new();

                    for arg in args.tail().iter() {
                        argstr.push_str(arg.as_slice());
                        argstr.push_str(" ");
                    }

                    match repl.read(argstr.trim().to_string())
                        .and_then(|r| repl.eval(r))
                        .and_then(|r| repl.print(r)) {
                            Ok(_) => Ok(0),
                            Err(_) => Err(1),
                        }
                },
            }
        },
        None    => { println!("{}", usage()); Ok(0) },
    };

    match result {
        Ok(o) =>  o as isize,
        Err(e) => e as isize,
    }
}

pub fn main() {
    os::set_exit_status(run(&os::args()));
}

#[cfg(test)]
mod tests {
    #[test]
    #[should_fail]
    fn test_run_noargs() {
        let empty = Vec::<String>::new();
        let ec = super::run(&empty);
        assert!(0 == ec);
    }

    #[test]
    fn test_run_version() {
        let mut args = Vec::new();
        args.push_all(&["rub".to_string(),
                        "--version".to_string()]);
        let ec = super::run(&args);
        assert!(0 == ec);
    }

    #[test]
    fn test_run_help() {
        let mut args = Vec::new();
        args.push("rub".to_string());
        args.push("-h".to_string());
        let mut ec = super::run(&args);
        assert!(0 == ec);

        args.pop();
        args.push("--help".to_string());
        ec = super::run(&args);
        assert!(0 == ec);
    }
}
